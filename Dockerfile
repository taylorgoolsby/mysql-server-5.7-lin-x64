FROM node:10

# libaio and libnuma need to be installed in order to run mysqld.
RUN apt-get update
RUN apt-get install libaio-dev -y
RUN apt-get install libnuma-dev -y

# Test 1:
#COPY . /
#RUN yarn
#RUN npm test

# Test 2:
COPY entrypoint.sh /entrypoint.sh
COPY package.test.json /graphql/package.json
ENTRYPOINT ["/entrypoint.sh"]
#RUN cd graphql && yarn && npm run mysql

#RUN node index.js
#ENTRYPOINT ["./server/reinitialize.sh"]