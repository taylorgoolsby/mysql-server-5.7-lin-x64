#!/usr/bin/env bash

full_path=$(realpath $0)
dir_path=$(dirname $full_path)
data_path="$dir_path/data/mysql"
temp_dir="$dir_path/temp"
echo "pwd: $(pwd)"
echo "fullpath: $full_path"
echo "dir_path: $dir_path"
echo "temp_path: $temp_path"
echo "ls: $(ls)"
echo "$(head -30 $dir_path/my.cnf)"
rm -rf $dir_path/data/mysql/*
rm -rf $dir_path/binlog/*
$dir_path/mysqld --defaults-file=$dir_path/my.cnf --tmpdir=$temp_dir --explicit_defaults_for_timestamp --log_syslog=0 --initialize-insecure --user=root --socket=$temp_dir/mysql.sock --basedir=$dir_path --datadir=$data_path
$dir_path/mysqld --defaults-file=$dir_path/my.cnf --tmpdir=$temp_dir --explicit_defaults_for_timestamp --log_syslog=0 --console --user=root --socket=$temp_dir/mysql.sock --basedir=$dir_path --datadir=$data_path
