#!/usr/bin/env bash

full_path=$(realpath $0)
dir_path=$(dirname $full_path)
temp_dir="$dir_path/temp"
data_path="$dir_path/data/mysql"
$dir_path/mysqld --defaults-file=$dir_path/my.cnf --tmpdir=$temp_dir --explicit_defaults_for_timestamp --log_syslog=0 --console --user=root --socket=$temp_dir/mysql.sock --basedir=$dir_path --datadir=$data_path
